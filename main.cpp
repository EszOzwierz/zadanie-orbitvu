#include <iostream>
#include <stdlib.h>
#include <conio.h>
#include <ctime>
#include <fstream>
#include <sstream>
#include <direct.h>
#include <cstring>


using namespace std;

class User
{
private:
    string name;
    string emailAdress;
    string jobTitle;
    int age;

public:
    string getName()
    {   return name;    }
    void setName(string AddName)
    {   name=AddName;   }

    string getEmailAdress()
    {   return emailAdress;    }
    void setEmailAdress(string AddEmail)
    {   emailAdress=AddEmail;   }

    string getjobTitle()
    {   return jobTitle;    }
    void setjobTitle(string AddjobTitle)
    {   jobTitle=AddjobTitle;   }

    int getAge()
    {   return age;    }
    void setAge(int AddAge)
    {   age=AddAge;   }

};


void addNewUserToDatabase()
{
    User NewUser;
    fstream Database;
    string fileName;

    string NewName,NewEmail,NewTitle;
    int NewAge;


    cout<<"\nName: ";
    cin.ignore();
    getline(cin, NewName);
            while (NewName.empty())
            {
                    cin.ignore(256,'\n');
                    getline(cin, NewName);
            }
    NewUser.setName(NewName);
    cout<<"\nAge: ";
    cin>>NewAge;
    while(cin.fail())
        {
            cout<<"Wrong age input. Please input correct age in years."<<endl;
            cin.clear();
            cin.ignore(256,'\n');
            cin>>NewAge;
        }
    NewUser.setAge(NewAge);
    cout<<"\nEmail Adress: ";
    cin>>NewEmail;
    NewUser.setEmailAdress(NewEmail);
    cout<<"\nJob Title: ";
    cin.ignore();
    getline(cin, NewTitle);
    while (NewTitle.empty())
            {
                    cin.ignore(256,'\n');
                    getline(cin, NewTitle);
            }
    NewUser.setjobTitle(NewTitle);
    cout<<endl;

    fileName="Users\\"+NewUser.getjobTitle()+" - "+NewUser.getName()+".txt";
    Database.open(fileName.c_str(),ios::out | ios::app);
    Database<<NewUser.getName()<<"\n"<<NewUser.getAge()<<"\n"<<NewUser.getEmailAdress()<<"\n"<<NewUser.getjobTitle();

    Database.close();
    Database.clear();
}


void readFromDatabase()
{
    int row=1;
    fstream Database;
    string fileName,displayLine,NewTitle,NewName;
    string DataName,DataEmail,DataTitle;
    int DataAge;
    User UserData;

    cout<<"Display User with the title of: ";
    cin.ignore();
    getline(cin, NewTitle);
    while (NewTitle.empty())
    {
        cin.ignore();
        getline(cin, NewTitle);
    }
    UserData.setjobTitle(NewTitle);

    cout<<"Name: ";
    getline(cin,NewName);
    while (NewName.empty())
    {
        cin.ignore();
        getline(cin, NewName);
    }
    UserData.setName(NewName);


    fileName="Users\\"+UserData.getjobTitle()+" - "+UserData.getName()+".txt";
    Database.open(fileName.c_str(),ios::in);
    if(Database.good()!=false)
    {
        while(getline(Database,displayLine))
        {
            switch(row)
            {
            case 1:
                DataName=displayLine;
                break;
            case 2:
                DataAge=atoi(displayLine.c_str());
                break;
            case 3:
                DataEmail=displayLine;
                break;
            case 4:
                DataTitle=displayLine;
                break;
            }
            row++;
        }
        cout<<"\nName: "<<DataName<<"\nAge: "<<DataAge<<"\nEmail Adress: "<<DataEmail<<"\nJob Title: "<<DataTitle<<"\n"<<endl;

        Database.close();
        Database.clear();
    }
    else
    {
        cout<<"No such User found. Try again.";
        getch();
        system("cls");
        readFromDatabase();
    }
}


void exitProgram()
{
char quit;

cout<<"Do You really want to quit the program? Y/N: ";
cin>>quit;
if(quit=='y'||quit=='Y')
{
    cout<<"\n See You next time.";
    getch();
    cout<<"\n Bye, bye :)"<<endl;
    getch();
    exit(0);
}
else if(quit=='n'||quit=='N')
{
    system("cls");
}
else
{
    cout<<"\nNo option choosen. Returning to main screen.\Press any key to continue."<<endl;
    getch();
    system("cls");
}
}

void WelcomeMessage()
{
    cout<<"Welcome Boss,\nWhat do You want to do?\n"<<endl;
    cout<<"\t1. Add user to database"<<endl;
    cout<<"\t2. Display data of existing user"<<endl;
    cout<<"\t3. Exit"<<endl<<endl;
    cout<<"Option: ";
}

char chooseOption(char option)
{
        switch(option)
            {
            case '1':
            {
                cout<<"==== User Adding query ===="<<endl;
                cout<<"You will be asked about Name, Age, Email Adress and Job Title"<<endl;
                addNewUserToDatabase();
            }
            break;

            case '2':
            {
                cout<<"=== User Data ==="<<endl;
                cout<<"(You will be asked for Job Title and a Name...\n"<<endl;
                readFromDatabase();
            }
            break;

            case '3':
            {
                exitProgram();
            }
            break;
            }
}
void MainMenu(char option,char continuation)
{
    system("mkdir \"Users\"");
    system("cls");
    do
    {
        WelcomeMessage();

        cin>>option;
        system("cls");

        if(option=='1' || option=='2' || option=='3')
        {
            chooseOption(option);
        }
        else
        {
            cout<<"Choose one from existing options!"<<endl;
            getch();
            system("cls");
        }

        cout<<"\n==> Do You want to do someting else?\n\nEnter Y if You want to continue,\nEnter N to quit the program: ";
        cin>>continuation;
        system("cls");
        if(continuation=='y' || continuation=='Y')
        {
            continue;
        }
        else if(continuation=='n' || continuation=='N')
        {
            exitProgram();
        }
    }
    while (continuation=='y' || continuation=='Y');

}

int main()
{
    char option,continuation;

    MainMenu(option,continuation);

    return 0;
}
